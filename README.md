# Solutions for Advent Of Code for Problem 1, Problem 4, and Problem 10
This set of problems are solved on the basis of OOPs concept of python. 

##### Commands to execute each part of question

###### Part 1

```
python q1.py

```

###### Part 2

```
python q2.py

```

For each part of question tests are written using ```unittest library```

##### Following commands are executed to run the tests:

###### For part 1 of the question

```
 $ python -m unittest q1_test.py

```
###### For part 2 of the question

```
 $ python -m unittest q2_test.py

```

Code coverage reports are also generated for each part of program

##### Following commands are generate to code coverage and code coverage report for the code:

###### Code Coverage Command

```
 $ coverage run -m unittest q1.py q2.py q1_test.py q2_test.py

```
###### Code Coverage  Report Command 

```
 $ coverage report

```
###### Code Coverage HTML  Report Command 

```
 $ coverage html

```

